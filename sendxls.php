<?php
	function parse_excel_file($filename, $row_count, $column_count){
		$result = array();
		$file_type = PHPExcel_IOFactory::identify($filename);
		$objReader = PHPExcel_IOFactory::createReader($file_type);
		$objPHPExcel = $objReader -> load($filename);
		$excel = PHPExcel_IOFactory::load($filename);
		$excel -> setActiveSheetIndex(0);
		$sheet = $excel -> getActiveSheet();
		$highestColumnIndex = PHPExcel_Cell::columnIndexFromString($column_count);
		$highestColumnIndex -= 2;
		for ($i = 2; $i < $row_count; ++$i){
			for ($j = 1; $j > -1; --$j){
				$temp = $sheet -> getCellByColumnAndRow($j, $i) -> getValue();
				array_push($result, $temp);
			}
			$temp = $sheet -> getCellByColumnAndRow($highestColumnIndex, $i) -> getValue();
			array_push($result, $temp);
		}
		return $result;
	}
	function swapper($result){
		for ($i = 2; $i <= count($result); $i += 3){
			for($j = $i + 3; $j <= count($result); $j += 3){
				if ($result[$j] > $result[$i]){
					$temp = $result[$j]; $result[$j] = $result[$i]; $result[$i] = $temp;
					$temp = $result[$j - 1]; $result[$j - 1] = $result[$i - 1]; $result[$i - 1] = $temp;
					$temp = $result[$j - 2]; $result[$j - 2] = $result[$i - 2]; $result[$i - 2] = $temp;
				}
			}
		}
		return $result;
	}
	function create_excel_file($res, $row_count){
		$document = new PHPExcel();
		$sheet = $document -> setActiveSheetIndex(0);
		$sheet -> setCellValueByColumnAndRow(0, 1, 'Отчёт группы по журналу');
		$sheet -> mergeCells("A1:D1");
		$sheet -> setCellValueByColumnAndRow(0, 2, 'Фамилия');
		$sheet -> setCellValueByColumnAndRow(1, 2, 'Имя, отчество');
		$sheet -> setCellValueByColumnAndRow(2, 2, 'Баллы');
		$sheet -> getColumnDimension('A') -> setWidth(15);
		$sheet -> getColumnDimension('B') -> setWidth(30);
		$sheet -> getColumnDimension('C') -> setWidth(10);
		$sheet -> getColumnDimension('D') -> setWidth(40);
		for ($i = 3; $i <= $row_count; ++$i){
			for ($j = 0; $j < 3; ++$j){
				$temp = array_shift($res);
				$sheet -> setCellValueByColumnAndRow($j, $i, $temp);		
				$sheet -> setCellValueByColumnAndRow(3, 3, 'Лучший студент в группе');
			}
		}
		header ("Expires: Mon, 1 Apr 1974 05:00:00 GMT");
		header ("Last-Modified: " . gmdate("D,d M YH:i:s") . " GMT");
		header ("Cache-Control: no-cache, must-revalidate");
		header ("Pragma: no-cache");
		header ("Content-type: application/vnd.ms-excel");
		header ("Content-Disposition: attachment; filename=result.xls");
		$objWriter = new PHPExcel_Writer_Excel5($document);
		$objWriter->save('php://output');
		$objWriter->save("result.xls");
	}
	ini_set('display_errors',1); error_reporting(E_ALL);
	require_once('PHPExcel.php'); require_once('PHPExcel/Writer/Excel5.php');
    require_once('PHPExcel/IOFactory.php');
	$filename = $_FILES['filename']['tmp_name'];
	move_uploaded_file($_FILES['filename']['tmp_name'], $filename);
	$excel = PHPExcel_IOFactory::load($filename);
	$excel -> setActiveSheetIndex(0);
	$sheet = $excel -> getActiveSheet();
	Foreach($excel -> getWorksheetIterator() as $worksheet){
		$worksheet -> toArray();
	}
	$row_count = $worksheet -> getHighestRow();
	$column_count = $worksheet -> getHighestColumn();
	$result = parse_excel_file($filename, $row_count, $column_count);
	$res = swapper($result);
	create_excel_file($res, $row_count);
?>